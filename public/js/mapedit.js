let lat = document.getElementById("lat");
let lng = document.getElementById("lng");
let zoom = document.getElementById("zoom");

let northEast_lat = document.getElementById("northEast_lat");
let northEast_lng = document.getElementById("northEast_lng");
let southWest_lat = document.getElementById("southWest_lat");
let southWest_lng = document.getElementById("southWest_lng");

var map = L.map("map");
map.fitBounds([
  [northEast_lng.value, northEast_lat.value],
  [southWest_lng.value, southWest_lat.value],
]);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution: "",
}).addTo(map);

let marker = L.marker([lat.value, lng.value]).addTo(map);

map.on("click", (e) => {
  if (marker) {
    map.removeLayer(marker);
  }
  marker = L.marker([lat.value, lng.value]).addTo(map);
  lat.value = e.latlng.lat;
  lng.value = e.latlng.lng;
  zoom.value = e.target._zoom;

  northEast_lat.value = map.getBounds().getNorth();
  northEast_lng.value = map.getBounds().getEast();
  southWest_lat.value = map.getBounds().getSouth();
  southWest_lng.value = map.getBounds().getWest();
});
