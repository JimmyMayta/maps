<?php
namespace App\Controllers;

class Inicio extends BaseController{
  public function index(){
    $db = \Config\Database::connect();
    $builder = $db->table('ubicacion');
    $query = $builder->getWhere(['estado' => 1]);
    //$query = $builder->get();
    return view('mapas/mapas', ['query' => $query]);
  }

  public function crear(){
    return view('mapas/mapas_crear');
  }

  public function guardar(){
    $lat = $this->request->getPost('lat');
    $lng = $this->request->getPost('lng');
    $zoom = $this->request->getPost('zoom');

    $northEast_lat = $this->request->getPost('northEast_lat');
    $northEast_lng = $this->request->getPost('northEast_lng');
    $southWest_lat = $this->request->getPost('southWest_lat');
    $southWest_lng = $this->request->getPost('southWest_lng');

    if (!($lat && $lng && $zoom)) {
      return redirect('mapas');
    }

    $db = \Config\Database::connect();
    $builder = $db->table('ubicacion');
    $data = [
      'lat' => $lat,
      'lng' => $lng,
      'zoom' => $zoom,
      'northEast_lat' => $northEast_lat,
      'northEast_lng' => $northEast_lng,
      'southWest_lat' => $southWest_lat,
      'southWest_lng' => $southWest_lng,
      'estado' => 1,
    ];
    $builder->insert($data);
    return redirect('mapas');
  }

  public function editar($num){
    $db = \Config\Database::connect();
    $builder = $db->table('ubicacion');
    $query = $builder->getWhere(['id' => $num]);
    $query = $query->getResult();

    $res = [
      'id' => $query[0]->id,
      'lat' => $query[0]->lat,
      'lng' => $query[0]->lng,
      'zoom' => $query[0]->zoom,
      'northEast_lat' => $query[0]->northEast_lat,
      'northEast_lng' => $query[0]->northEast_lng,
      'southWest_lat' => $query[0]->southWest_lat,
      'southWest_lng' => $query[0]->southWest_lng,
      'estado' => 1
    ];
    return view('mapas/mapas_editar', $res);
  }

  public function editarguardar(){
    $id = $this->request->getPost('id');

    $lat = $this->request->getPost('lat');
    $lng = $this->request->getPost('lng');
    $zoom = $this->request->getPost('zoom');

    $northEast_lat = $this->request->getPost('northEast_lat');
    $northEast_lng = $this->request->getPost('northEast_lng');
    $southWest_lat = $this->request->getPost('southWest_lat');
    $southWest_lng = $this->request->getPost('southWest_lng');

    if (!($lat && $lng && $zoom)) {
      return redirect('mapas');
    }

    $db = \Config\Database::connect();
    $builder = $db->table('ubicacion');
    $data = [
      'lat' => $lat,
      'lng' => $lng,
      'zoom' => $zoom,
      'northEast_lat' => $northEast_lat,
      'northEast_lng' => $northEast_lng,
      'southWest_lat' => $southWest_lat,
      'southWest_lng' => $southWest_lng,
      'estado' => 1
    ];
    $builder->set($data);
    $builder->where('id', $id);
    $builder->update();
    return redirect('mapas');
  }

  public function eliminar($num){
    $db = \Config\Database::connect();
    $builder = $db->table('ubicacion');
    $builder->set('estado', 2);
    $builder->where('id', $num);
    $builder->update();
    return redirect('mapas');
  }
}









