<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="<?= base_url('css/leaflet.css') ?>"/>
  <link rel="stylesheet" href="<?= base_url('css/map.css') ?>"/>
  <script src="<?= base_url('js/leaflet.js') ?>"></script>
</head>
<body>
  <h1>Mapas editar</h1>
    <a href="<?= route_to('mapas') ?>">Mapas</a>
    <br><br>
    <form action="<?= route_to('editarguardar') ?>" method="post">
      <input id="id" type="text" name="id" value="<?= $id ?>">
      <input id="lat" type="text" name="lat" value="<?= $lat ?>">
      <input id="lng" type="text" name="lng" value="<?= $lng ?>">
      <input id="zoom" type="text" name="zoom" value="<?= $zoom ?>">

      <input id="northEast_lat" type="text" name="northEast_lat" value="<?= $northEast_lng ?>">
      <input id="northEast_lng" type="text" name="northEast_lng" value="<?= $northEast_lat ?>">
      <input id="southWest_lat" type="text" name="southWest_lat" value="<?= $southWest_lng ?>">
      <input id="southWest_lng" type="text" name="southWest_lng" value="<?= $southWest_lat ?>">

      <div id="map" style="border: 1px solid black"></div>
    <button type="submit">Guardar</button>
  </form>
    <script src="<?= base_url('js/mapedit.js') ?>"></script>
</body>
</html>










